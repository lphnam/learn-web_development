<?php
  $text   = "\t\tThese are a few words :) ... ";
  $binary = "\x09Example string\x0A";
  $hello  = "Hello World";

  var_dump($text, $binary, $hello);

  print("\n");

  $trimmed = trim($text);
  var_dump($trimmed);

  $trimmed = trim($text, " \t.");
  var_dump($trimmed);

  $trimmed = trim($hello, "Hdle");
  var_dump($trimmed);

  $trimmed = trim($hello, "HdWr");
  var_dump($trimmed);
