<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lessons;
use App\User;

class LessonsController extends Controller
{
    // protected $lessonTransformer;
    //
    // function __construct(LessonTransformer $lessonTransformer)
    // {
    //   $this->lessonTransformer = $lessonTransformer;
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lessons = Lessons::all();
        return response()->json([
          'data' => $this->transformCollection($lessons)
        ], 200);

        // return $lessons;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lesson = Lessons::find($id);

        if (! $lesson) {
          return response()->json([
            'error' => [
              'message' => 'Lesson does not exist'
            ],
          ], 404);
        }

        return response()->json([
          'data' => $this->transform($lesson)
        ], 202);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function transformCollection($lessons) {
      return array_map([$this, 'transform'], $lessons->toArray());
    }

    private function transform($lesson) {
      return [
        'title' => $lesson['title'],
        'body'  => $lesson['body'],
        'active' => (boolean) $lesson['some_bool']
      ];
    }
}
