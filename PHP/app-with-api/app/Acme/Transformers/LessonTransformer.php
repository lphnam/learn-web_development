<?php

namespace Acme\Transformers;

class LessonTransformer extends Transformer
{
    /**
    * Transform a collection of lessons
    *
    * @param $lessons
    * return array
    */

    public function transform($lesson) {
      return [
        'title' => $lesson['title'],
        'body'  => $lesson['body'],
        'active' => (boolean) $lesson['some_bool']
      ];
    }
}
