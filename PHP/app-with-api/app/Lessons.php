<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lessons extends Model
{
    protected $fillable = [
        'title', 'body'
    ];
    // 
    // protected $hidden = [
    //     'title'
    // ];
}
