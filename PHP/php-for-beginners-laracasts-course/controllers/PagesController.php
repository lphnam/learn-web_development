<?php
class PagesController
{
  public function home()
  {
    // Receive the request.
    // Delegate
    // Return a response
    return view('index');
  }

  public function about()
  {
    require view('about');
  }

  public function contact()
  {
    require view('contact');
  }
}

?>
