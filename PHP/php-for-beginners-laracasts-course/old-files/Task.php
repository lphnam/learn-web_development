<?php
  class Task {
    public $description;
    public $completed;

    public function complete()
    {
      # code...
      $this->completed = true;
    }

    public function isCompleted()
    {
      return $this->completed;
    }
  }
?>
