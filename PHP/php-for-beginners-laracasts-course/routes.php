<?php
  // $router->define([
  //   '' => 'controllers/index.php',
  //   'about' => 'controllers/about.php',
  //   'about/culture' => 'controllers/about-culture.php',
  //   'contact-our-company' => 'controllers/contact.php',
  //   'names' => 'controllers/add-name.php'
  // ]);

  $router->get('', 'PagesController@home');
  $router->get('about', 'PagesController@about');
  // $router->get('about/culture', 'controllers/about-culture.php');
  $router->get('contact-our-company', 'PagesController@contact');

  // $router->post('names', 'controllers/add-name.php');
  $router->get('users', 'UsersController@index');
  $router->post('users', 'UsersController@store');

?>
