@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <a href="#"> {{ $thread->creator->name }} </a> posted:
                  {{ $thread->title }}
                </div>

                <div class="panel-body">
                      <article>
                          {{$thread->body}}
                      </article>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <?php foreach ($thread->replies as $reply): ?>
              @include ('threads.reply')
            <?php endforeach; ?>
        </div>
    </div>

    @if (auth()->check())
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <form action="{{ $thread->path() .'/replies' }}" method="post">
              {{ csrf_field() }}

              <textarea name="body" id="body" class="form-control" rows="5" cols="80" placeholder="Have something to say?">
              </textarea>

              <button type="submit" class="btn btn-default" name="button">Post</button>
            </form>
          </div>
      </div>
    @else
      <p class="text-center">Please <a href="{{ route('login') }}">sign in</a> to participate in this discusstion.</p>
    @endif
</div>
@endsection
