<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ReadThreadsTest  extends TestCase
{
    use DatabaseMigrations;

    function setUp()
    {
      parent::setUp();
      $this->thread = factory('App\Thread')->create();
    }

    /** @test */
    function a_user_can_view_all_threads()
    {
        $response = $this->get('/threads');
        $response->assertStatus(200);
        $response->assertSee($this->thread->title);
    }

    /** @test */
    function a_user_can_read_single_thread()
    {
        $response = $this->get($this->thread->path());
        $response->assertStatus(200);
        $response->assertSee($this->thread->title);
    }

    /** @test */
    function a_user_can_read_replies_that_are_associated_with_a_thread()
    {
      // Give we have a thread
      // And that thread includes replies
      $reply = factory('App\Reply')->create(['thread_id' => $this->thread->id]);

      // When we visit a thread page
      // Then we should see the replies.
      $response = $this->get($this->thread->path())
                      ->assertSee($reply->body);
    }

    /** @test */
    function a_user_can_filter_threads_according_to_a_channel()
    {
      $channel = create('App\Channel');
      $threadInChannel = create('App\Thread', ['channel_id' => $channel->id]);
      $threadNoInChannel = create('App\Thread');
      $this->get('/threads/' .$channel->slug)
          ->assertSee($threadInChannel->title)
          ->assertDontSee($threadNoInChannel->title);
    }

    /** @test */
    function a_user_can_filter_threads_by_any_username()
    {
      $this->signIn(create('App\User', ['name' => 'JohnDoe']));

      $threadByJohn = create('App\Thread', ['user_id' => auth()->id()]);
      $threadByOther = create('App\Thread');

      $this->get('threads?by=JohnDoe')
          ->assertSee($threadByJohn->title)
          ->assertDontSee($threadByOther->title);
    }
}
