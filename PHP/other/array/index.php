<?php
  /***
  * array_filter
  * array_map
  * array_column
  ***/
class Post
{
  public $title;

  public $published;

  public function __construct($title, $published) {
    $this->title = $title;
    $this->published = $published;
  }
}

$posts = [
  new Post('My First Post', true),
  new Post('My Second Post', true),
  new Post('My Third Post', true),
  new Post('My Fourth Post', false)
];

// This is a example for array_filter
// $unpublishedPosts = array_filter($posts, function($post) {
//   return !$post->published;
// });
//
// echo '<pre>';
// var_dump($unpublishedPosts);
// echo '</pre>';

// This is a example for array_filter
// $modified = array_map(function ($post) {
//   return 'foobar';
// }, $posts);

// $title = array_map(function ($post) {
//   return $post->title;
// }, $posts);

// $titles = array_column($posts, 'title');


var_dump($posts);
?>
