function Clock(hours, minutes, seconds, ctx, radius) {
    this.hours = hours;
    this.minutes = minutes;
    this.seconds = seconds;
    this.ctx = ctx;
    this.radius = radius;
    this.fullHour = this.hours + ':' + this.minutes + ':' + this.seconds;
    var timer;

    this.updateTime = function () {
        this.seconds++;
        if (this.seconds > 59) {
            this.seconds = 0;
            this.minutes++;
            if (this.minutes > 59) {
                this.minutes = 0;
                this.hours++;
                if (this.hours > 23) {
                    this.hours = 0;
                }
            }
        };
    };

    this.stopClock = function() {  
        var self = this;    
        clearInterval(timer);
        console.log('stop');
    }

    // ---------- run 1 time ----------
    // this.startClock = function() {
    //     var draw = this.drawClock();
    //     setInterval(function() {
    //         draw
    //     }, 1000);
    // }

    this.startClock = function() {
        var self = this;
        timer = setInterval(function() {
            self.drawClock();
        }, 1000);
    }

    this.drawClock = function () { 
        this.drawFace();
        this.drawNumbers();
        this.drawTime();
        this.updateTime();
        console.log('draw');
    }

    this.drawTime = function () {
        var hour = this.hours;
        var minute = this.minutes;
        var second = this.seconds;
        //hour
        hour = hour % 12;
        hour = (hour * Math.PI / 6) + (minute * Math.PI / (6 * 60)) + (second * Math.PI / (360 * 60));
        this.drawHand(hour, this.radius * 0.4, 6);
        // minute
        minute = (minute * Math.PI / 30) + (second * Math.PI / (30 * 60));
        this.drawHand(minute, this.radius * 0.65, 6);
        // second
        second = (second * Math.PI / 30);
        this.drawHand(second, this.radius * 0.75, 2);
    }

    this.drawHand = function (pos, length, width) {
        this.ctx.beginPath();
        this.ctx.lineWidth = width;
        this.ctx.lineCap = "round";
        this.ctx.moveTo(0, 0);
        this.ctx.rotate(pos);
        this.ctx.lineTo(0, -length);
        this.ctx.stroke();
        this.ctx.rotate(-pos);
    }

    this.drawFace = function () {
        var grad;
        this.ctx.beginPath();
        this.ctx.arc(0, 0, this.radius, 0, 2 * Math.PI);
        this.ctx.fillStyle = 'white';
        this.ctx.fill();

        grad = this.ctx.createRadialGradient(0, 0, this.radius * 0.95, 0, 0, this.radius * 1.05);
        grad.addColorStop(0, '#333');
        grad.addColorStop(0.5, 'white');
        grad.addColorStop(1, '#333');
        this.ctx.strokeStyle = grad;
        this.ctx.lineWidth = this.radius * 0.1;
        this.ctx.stroke();

        this.ctx.beginPath();
        this.ctx.arc(0, 0, this.radius * 0.1, 0, 2 * Math.PI);
        this.ctx.fillStyle = '#333';
        this.ctx.fill();
    }

    this.drawNumbers = function () {
        var ang;
        var num;
        this.ctx.font = this.radius * 0.12 + "px arial";
        this.ctx.textBaseline = "middle";
        this.ctx.textAlign = "center";
        for (num = 1; num < 13; num++) {
            ang = num * Math.PI / 6;
            this.ctx.rotate(ang);
            this.ctx.translate(0, -this.radius * 0.85);
            this.ctx.rotate(-ang);
            this.ctx.fillText(num.toString(), 0, 0);
            this.ctx.rotate(ang);
            this.ctx.translate(0, this.radius * 0.85);
            this.ctx.rotate(-ang);
        }
    }

};

function newClock() {
    var timezone = document.getElementById('timezone');
    var place = timezone.options[timezone.selectedIndex].value;

    var newCl = document.getElementById('new-clock');
    var newZone = document.createElement('div');
    newCl.appendChild(newZone);

    var title = document.createElement("p");
    title.innerText = timezone.options[timezone.selectedIndex].text;
    newZone.appendChild(title);

    var newCanvas = document.createElement("canvas");
    newCanvas.width = 200;
    newCanvas.height = 200;
    newZone.appendChild(newCanvas);


    var btnStop = document.createElement("button");
    btnStop.innerText = 'Stop';
    newZone.appendChild(btnStop);


    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", 'https://script.google.com/macros/s/AKfycbyd5AcbAnWi2Yn0xhFRbyzS4qMq1VucMVgVvhul5XqS9HkAyJY/exec?tz=' + place);
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                var data = JSON.parse(xmlhttp.responseText);
                var ctx = newCanvas.getContext("2d");
                var radius = newCanvas.height / 2;
                ctx.translate(radius, radius);
                radius = radius * 0.90;
                var newClock = new Clock(data.hours, data.minutes, data.seconds, ctx, radius);
                newClock.startClock();
                btnStop.addEventListener("click", newClock.stopClock);
            } else {
                console.log('Error: ' + xmlhttp.statusText)
            }
        }
    }

    xmlhttp.send();
}

