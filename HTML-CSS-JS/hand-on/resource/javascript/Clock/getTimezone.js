function getTimezoneDb() {
  // ***********************************************************************************************
  //
  // timezones: object store data response from api
  // timezones = { status, message, zones=[{countryCode,countryName,zoneName,gmtOffset,timestamp}] }
  // After get timezones, send it to myFunction(timezones.zones) ~ timezones.zone is object
  //
  // ***********************************************************************************************

  console.log("Connected to getTimezoneDb");
  var timezones;
  var xhttp = new XMLHttpRequest();

  xhttp.open("GET", "http://api.timezonedb.com/v2/list-time-zone?key=OH7SRXTRGTSS&format=json");
  xhttp.onreadystatechange = function() {
    console.log("readyState: ", xhttp.readyState);
    if (xhttp.readyState == XMLHttpRequest.DONE) {
      // XMLHttpRequest.DONE same xhttp.readyState = 4
      if (xhttp.status === 200)  {
        // status = 200, mean is "the request is OK"
        // read it more with HTTP Message
        var timezones = JSON.parse(xhttp.responseText);
        myFunction(timezones.zones);
      }
      else {
        console.log('Error: ' + xhttp.statusText);
      }
    }
  }
  xhttp.send();
}
