// var userName = prompt("What is your name?");
// alert("Nice to meet you, " + userName);
// console.log("Also great to meet you, " + userName);

// var firstName = prompt("What is your first name?");
// var lastName = prompt("What is your last name?");
// var age = prompt("How old are your?");
//
// var fullName = firstName + " " + lastName;
// console.log("Your full name is " + fullName);
// console.log("You are " + age + " years old");

var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
var colors = ["red", "orange", "yellow", "green"];

console.log('numbers: ',numbers);
numbers.forEach(function(color){
  if (color % 3 === 0) {
    console.log(color);
  }
});

//-- exercise find max
var numbArr = [32, 12, 89, 1, 109, 23];

function max(numbArr) {
  var maxNumb = numbArr[0];

  numbArr.forEach(function(numb){
    if (numb > maxNumb) {
      maxNumb = numb;
    }
  });
  return maxNumb;
}

function sumArr(numbArr) {
  var sum = 0;

  numbArr.forEach(function(numb){
      sum += numb;
  });
  return sum;
}

console.log('numbArr: ', numbArr);
console.log('max numb is ', max(numbArr));
console.log('sum arr is ', sumArr(numbArr));
