// Check Off Specific Todos By Clicking
$("ul").on("click", "li", function() {
  // Condition core
  //---
  //if li is gray
  //turn it black
  //else
  //turn is gray
  // !!!
  // if ($(this).css("color") === "rgb(128, 128, 128)") {
  //   $(this).css({
  //     color: "black",
  //     textDecoration: "none"
  //   });
  // } else {
  //   $(this).css({
  //     color: "gray",
  //     textDecoration: "line-through"
  //   });
  // }
  //!----

  $(this).toggleClass("completed");

});

// Click on X to deleto Todo
$("ul").on("click", "span", function(event) {
  $(this).parent().fadeOut(500, function(){
    $(this).remove();
  });
  // event.stopPropagation();
});

$("input[type='text']").keypress(function(event) {
  if (event.which === 13) {
    //grabbing new todo text from input
    var todoText = $(this).val()
    $(this).val("");
    //create a new li and add to ul
    $("ul").append("<li><span><i class=\"fa fa-trash\"></i></span> "+ todoText + "</li>")
  }
});

$("body").click(function(e){
  $(".fa-plus").click(function(){
    $("input[type=\"text\"]").fadeToggle();
  });

  $("input[type=\"text\"]").mousedown(function(){
    console.log("click");
  });
  var target = e.srcElement || e.target;
  console.log(target);
  console.log($("input[type=\"text\"]").html());
});
