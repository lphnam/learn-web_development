function Clock(zoneName) {
  this.name = zoneName;
  this.data = {};
  this.hours = "";
  this.mins = "";
  this.secs = "";

  this.setTime = function() {
    var curTime = this.data.formatted;
    this.hours = Number(curTime.substr(11, 2));
    this.mins = Number(curTime.substr(14, 2));
    this.secs = Number(curTime.substr(17, 2));
  }

  this.updateTime = function(self) {
    var element = document.getElementById("showClock");
    var node = document.createElement("li");
    node.setAttribute("id", self.name);
    console.log(self.id);

    element.appendChild(node);

    setInterval(function() {
      self.secs += 1;
      if (self.secs === 60) {
        self.mins += 1;
        self.secs = 0;

        if (self.mins === 60) {
          self.secs = 0;
          self.mins = 0;
          self.hours += 1;

          if (self.hours === 24) {
            self.secs = 0;
            self.mins = 0;
            self.hours = 0;
          }
        }
      }

      var timeString = self.hours + " : " + self.mins + " : " + self.secs;
      if (self.hours < 10) {
        timeString = '0' + self.hours + " : " + self.mins + " : " + self.secs;
      }
      if (self.mins < 10) {
        timeString = self.hours + " : " + '0' + self.mins + " : " + self.secs;
      }
      if (self.secs < 10) {
        timeString = self.hours + " : " + self.mins + " : " + '0' + self.secs;
      }
      node.textContent = self.name + " now is " + timeString;
    }, 1000);
  }

  this.getDataFromDB = function(zone, myClock) {
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == XMLHttpRequest.DONE) {
        // XMLHttpRequest.DONE same xhttp.readyState = 4
        if (xhttp.status === 200)  {
          // status = 200, mean is "the request is OK"
          // read it more with HTTP Message
          console.log("readyStatus: " + xhttp.status + "readyState" + xhttp.readyState);

          data = JSON.parse(xhttp.responseText);
          onAjaxSuccess: {
            myClock.data = data;
            myClock.setTime();
            myClock.updateTime(myClock)
          }
        }
        else {
          console.log('Error: ' + xhttp.statusText);
        }
      }
    }

    xhttp.open("GET", "http://api.timezonedb.com/v2/get-time-zone?key=OH7SRXTRGTSS&format=json&by=zone&zone=" + zone);
    xhttp.send();
  }
}
