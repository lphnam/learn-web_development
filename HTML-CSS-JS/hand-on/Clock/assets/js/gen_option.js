function generator(data) {
  // receive object zones from api - data
  // insert zoneName options to select

  var element = document.getElementById("selectZone");

  for (var i = 0; i < data.length; i++) {
    var node = document.createElement("option");
    node.text = data[i].zoneName;
    node.value = data[i].zoneName;

    element.appendChild(node);
  }
}
